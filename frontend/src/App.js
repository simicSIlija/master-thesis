import ConversationalAgent from "./pages/ConversationalAgent";
import NoIntervention from "./pages/NoIntervention";
import StaticIntervention from "./pages/StaticIntervention";
import ValidationPage from "./pages/ValidationPage";
import Home from "./pages/Home";
import Error from "./pages/Error";
import { Switch, Route } from 'react-router-dom';
import ValidationFailed from "./pages/ValidationFailed";
import FinishSuccess from "./pages/FinishSuccess";
function App() {
  return (
    <div>
      <Switch>
        <Route path="/" component={Home} exact />
        <Route path="/valid" component={ValidationPage} />
        <Route path="/first" component={NoIntervention} />
        <Route path="/second" component={StaticIntervention} />
        <Route path="/third" component={ConversationalAgent} />
        <Route path="/validation/finish" component={ValidationFailed} />
        <Route path="/finish" component={FinishSuccess} />
        <Route component={Error} />

      </Switch>
    </div>

  );
}

export default App;
