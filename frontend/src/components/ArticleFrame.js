import {Document, Page, pdfjs} from "react-pdf";
import React, {useState, useEffect} from 'react';
import axios from 'axios';
import {Button} from "react-bootstrap";

import "../index.css"

pdfjs.GlobalWorkerOptions.workerSrc = `//cdnjs.cloudflare.com/ajax/libs/pdf.js/${pdfjs.version}/pdf.worker.js`;

function ArticleFrame({source, finishArticle}) {
    const [file, setFile] = useState({data: []});
    const [numPages, setNumPages] = useState(null);
    const [pageNumber, setPageNumber] = useState(1);

    function onDocumentLoadSuccess({numPages}) {
        setNumPages(numPages);
    }

    function changePage(offset) {
        setPageNumber(prevPageNumber => prevPageNumber + offset);
    }

    function previousPage() {
        changePage(-1);
    }

    function firstPage() {
        setPageNumber(1);
    }

    function lastPage() {
        setPageNumber(numPages);
        finishArticle();
    }

    function nextPage() {
        changePage(1);
        if (pageNumber === numPages - 1) {
            finishArticle();
        }
    }

    useEffect(() => {
        axios.get(source, {responseType: 'arraybuffer'}).then(
            response => {
                setFile({data: response.data});
            }
        );
    }, [source])

    return (
        <div>
            <div className={"PageButtons"}>
                <p>
                    Page {pageNumber || (numPages ? 1 : '--')} of {numPages || '--'}
                </p>
                <Button
                    variant="light"
                    disabled={pageNumber === 1}
                    onClick={firstPage}
                >
                    First page
                </Button>
                <Button
                    variant="light"
                    disabled={pageNumber <= 1}
                    onClick={previousPage}
                >
                    Previous
                </Button>
                <Button
                    variant="light"
                    disabled={pageNumber >= numPages}
                    onClick={nextPage}
                >
                    Next
                </Button>
                <Button
                    variant="light"
                    disabled={pageNumber === numPages}
                    onClick={lastPage}
                >
                    Last Page
                </Button>
                <Button variant="light" href={source} target="_blank" rel="noopener noreferrer">Download PDF</Button>
            </div>
            <Document
                file={file}
                onLoadSuccess={onDocumentLoadSuccess}
                //onLoadError={console.error}
            >
                <Page pageNumber={pageNumber}/>
            </Document>


        </div>
    );
}

export default ArticleFrame;