import MessageList from './MessageList'
import MessageSend from './MessageSend'
import useChat from './useChat';
import SubmitAnswer from "./SubmitAnswer";

function ChatComponent({intervention}) {
    const room_id = intervention.id;
    const user_id = intervention.user.name;

    const {messages, sendMessage, finishConversation} = useChat({room_id, user_id});
    const submitMessage = (text) => {
        sendMessage(text);
    };

    return (
        <div>
            <MessageList messages={messages}/>
            {!finishConversation ? <MessageSend submitMessage={submitMessage}/> :
                <SubmitAnswer intervention={intervention}/>}
        </div>
    );
}

export default ChatComponent;