import {ListGroup} from "react-bootstrap";
import MessageListItem from "./MessageListItem";
import {useEffect, useRef} from "react";
const listStyles = {
    height: '80vh',
    border: '1px solid rgba(0,0,0,.4)',
    borderRadius: '4px',
    overflow: 'auto',
    background: '#ECE5DD'
}

function MessageList({messages}) {
    const messagesEndRef = useRef(null)
    useEffect(() => {
        messagesEndRef.current?.scrollIntoView({
            behavior: 'smooth'
        })
    }, [messages])
    return (
        <div>
            <ListGroup variant='flush' style={listStyles}>
                {messages.map((message, i) => <MessageListItem id={i} message={message}/>)}
                <span ref={messagesEndRef}/>
            </ListGroup>
        </div>
    );
}

export default MessageList;