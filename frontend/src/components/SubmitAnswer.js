import {useState} from "react";
import {Button, Form} from "react-bootstrap";
import QuestionAnswer from "./QuestionAnswer";
import restUtils from "./RestUtils"
import {useHistory} from 'react-router-dom';
import {NotificationContainer, NotificationManager} from "react-notifications";


function SubmitAnswer({intervention}) {
    let history = useHistory();
    const interventionId = intervention.id;

    const [answer, setAnswer] = useState("LOW");
    const [explanation, setExplanation] = useState("");

    const updateAnswer = entry => {
        setAnswer(entry);
    }

    const submitAnswer = event => {
        event.preventDefault();
        const submitAnswerDto = {
            answer:answer,
            explanation:explanation
        }
        if (explanation.trim() === "") {
            NotificationManager.error('Please enter your explanation', 'You can not skip why field' , 2000);
            return;
        }
        restUtils.postAnswerIntervention(submitAnswerDto, interventionId)
            .then(
                () => history.replace({
                    pathname: "/finish"
                })
            )
            .catch(
                () => history.replace({
                    pathname: "/error"
                })
            )
    }

    const updateExplanation = event => {
        event.preventDefault();
        setExplanation(event.target.value);
    }

    return (
        <div>
            <Form onSubmit={submitAnswer}>
                <QuestionAnswer
                    question="How would you rate this article?"
                    answers={["LOW", "MEDIUM", "HIGH"]}
                    update={updateAnswer}/>
                <Form.Group controlId="exampleForm.ControlTextarea1">
                    <Form.Label>Why?</Form.Label>
                    <Form.Control as="textarea" rows={3}
                    value={explanation}
                    onChange={updateExplanation}/>
                </Form.Group>
                <Button type="submit">Submit answers</Button>
            </Form>
            <NotificationContainer/>
        </div>
    );
}

export default SubmitAnswer;