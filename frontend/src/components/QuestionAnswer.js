function QuestionAnswer({ question, answers, update }) {

    const selectAnswer = event => {
        event.preventDefault();
        update(event.target.value);
    }

    return (
        <div>
            <p>{question}</p>

            <p><select onChange={selectAnswer} name="answer" id="answer">
                {answers.map(answer => <option value={answer} key={answer}>{answer}</option>)}
            </select>
            </p>

        </div>
    );
}

export default QuestionAnswer;