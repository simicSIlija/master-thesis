import {Card, ListGroupItem} from "react-bootstrap";
import {BiUser, FaRobot} from "react-icons/all";
import "../index.css"

function MessageListItem({id, message}) {
    const currentUser = message.userName !== 'Newsy';
    return (
        <ListGroupItem key={id}
                       className={`d-flex messageColor ${currentUser ? 'justify-content-end' : ''}`}
        >
            <Card
                className={`${currentUser ? 'myMessage' : 'agentMessage'}`}
                text='dark'

            >
                <Card.Header className='d-flex justify-content-between align-items-center'>
                    <Card.Text><span>{currentUser ? <BiUser/> : <FaRobot/>}{currentUser ? " Me" : " Newsy"}</span></Card.Text>
                </Card.Header>
                <Card.Body className='d-flex justify-content-between align-items-center'>
                    <Card.Text>{message.content}</Card.Text>
                </Card.Body>
            </Card>
        </ListGroupItem>

    );
}

export default MessageListItem;