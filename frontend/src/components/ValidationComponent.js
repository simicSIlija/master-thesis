import QuestionAnswer from './QuestionAnswer'
import React, {useEffect, useState} from 'react';
import axios from 'axios';
import {Button, Form} from 'react-bootstrap';
import {useHistory} from 'react-router-dom';

function ValidationComponent({intervention}) {
    let history = useHistory();
    const [topics, setTopics] = useState([]);
    const [firstImages, setFirstImages] = useState([]);
    const [sources, setSources] = useState([]);
    const [topic, setTopic] = useState("");
    const [firstImage, setFirstImage] = useState("");
    const [source, setSource] = useState("");
    const articleId = intervention.article.id;
    const articlePath = "api/validation/" + articleId;

    const validationPath = "api/validation/" + intervention.id;

    useEffect(() => {

        axios.get(articlePath).then(
            response => {
                setTopics(response.data.topics);
                setTopic(response.data.topics[0]);
                setSources(response.data.sources);
                setSource(response.data.sources[0]);
                setFirstImages(response.data.firstImages);
                setFirstImage(response.data.firstImages[0]);
            }
        );
    }, [articlePath]);

    const updateTopic = entry => {
        setTopic(entry);
    };

    const updatefirstImage = entry => {
        setFirstImage(entry);
    };

    const updateSource = entry => {
        setSource(entry);
    };

    const nextPage = intervention => {
        const settings = intervention.interventionSettings;
        if (settings === "NO_INTERVENTION") {
            return "/first";
        }
        if (settings === "STATIC_INTERVENTION") {
            return "/second";
        }
        if (settings === "CONVERSATIONAL_AGENT") {
            return "/third"
        }
        return "/error";
    };

    const submitAnswers = event => {
        event.preventDefault();
        axios.post(validationPath, createDto()).then(
            () => history.push({
                pathname: nextPage(intervention),
                state: {intervention: intervention}
            })
        ).catch(() => history.replace({
            pathname: '/validation/finish'
        }))
    };

    const createDto = () => {
        return {
            topic: topic,
            firstImage: firstImage,
            source: source
        };
    };

    return (
        <div>
            <Form onSubmit={submitAnswers}>
                <QuestionAnswer
                    question={"What is the topic of the article?"} answers={topics} update={updateTopic}
                />
                <QuestionAnswer
                    question={"What is the source of the article?"} answers={sources} update={updateSource}
                />
                <QuestionAnswer
                    question={" What is in the first picture of the article?"} answers={firstImages}
                    update={updatefirstImage}
                />
                <Button type="submit">Submit answers</Button>
            </Form>
        </div>

    );
}

export default ValidationComponent;