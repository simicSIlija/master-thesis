import {useEffect, useRef, useState} from "react";
import io from "socket.io-client";

const update_client = "updatechat";
const message_from_client = "sendchat";
const add_user = "adduser";
const dump_history = "dump_history";
const ENDPOINT = "http://newsagent.know-center.tugraz.at/";

function useChat({room_id, user_id,}) {
    const [messages, setMessages] = useState([]);
    const [finishConversation, setFinishConversation] = useState(false);
    const socket_ref = useRef();
    useEffect(() => {
        const join_dto = {
            room:room_id,
            userName:user_id
        };
        socket_ref.current = io(ENDPOINT, {
            transports: ['polling', 'websocket']
        });
        socket_ref.current.emit(add_user, join_dto);
        socket_ref.current.on(dump_history, (data) => {
            setMessages(messages => messages.concat(data));
        });
        socket_ref.current.on(update_client, (data) => {
            const new_message = {
                content: data.content,
                userName: data.userName
            };
            if(new_message.content === 'finishConversation'){
                setFinishConversation(true);
            }else {
                setMessages(messages => messages.concat(new_message));
            }
        });
        return () => socket_ref.current.disconnect();
    }, [room_id, user_id]);

    const sendMessage = (text) => {
        const json_object = {userName: user_id, message: text};
        socket_ref.current.emit(message_from_client, json_object);
    };
    return { messages, sendMessage, finishConversation };
}
export default useChat;
