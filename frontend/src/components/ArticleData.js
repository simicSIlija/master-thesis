import {ListGroup, ListGroupItem} from "react-bootstrap";

function ArticleData({article}) {
    function getIntroSentence() {
        return "You read an article about: " + article.topic;
    }

    function getStyleData() {
        return article.formalLanguageStyle ? "Style in this article is formal " : "Authors have used slang here";
    }

    function getPictureData() {
        if (article.numberOfPictures === "MORE") {
            return "Number of images in this article is high";
        }
        if (article.numberOfPictures === "ADEQUATE") {
            return "Number of images in this article is adequate";
        }

        return "Number of images in this article is low";
    }

    function getPerspectiveData() {
        if (article.perspective === "SINGLE") {
            return "Author addresses topic from single perspective";
        }
        return "Author addresses topic from multiple points of view";
    }

    function getSourceData() {
        return "Article was written by: " + article.source + ". According to Wikipedia, they are: " + article.sourceDescription + ".";
    }

    return (
        <div>
            <ListGroup>
                <ListGroupItem>{getIntroSentence()}</ListGroupItem>
                <ListGroupItem>{getStyleData()}</ListGroupItem>
                <ListGroupItem>{getPictureData()}</ListGroupItem>
                <ListGroupItem>{getPerspectiveData()}</ListGroupItem>
                <ListGroupItem>{getSourceData()}</ListGroupItem>
            </ListGroup>
        </div>
    );
}

export default ArticleData;