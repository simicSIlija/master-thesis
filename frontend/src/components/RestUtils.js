import axios from "axios";


function postAnswerIntervention(answerDto, id) {
    const path = "/api/interventions/" + id;
    return axios.post(path, answerDto);
}

const exp_object = {
    postAnswerIntervention: postAnswerIntervention
}

export default exp_object