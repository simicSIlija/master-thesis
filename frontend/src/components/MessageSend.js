import {useState} from "react";
import {Button, Form, FormControl, InputGroup} from "react-bootstrap";
import {FiSend} from 'react-icons/fi'
import {NotificationContainer, NotificationManager} from 'react-notifications';


function MessageSend({submitMessage}) {

    const [message, setMessage] = useState("");

    const handleChange = (event) => {
        event.preventDefault();
        setMessage(event.target.value);
    }

    const submitForm = () => {
        if (message.trim() === "") {
            NotificationManager.error('Please enter your message', 'You can not send empty message', 2000);
            return;
        }
        submitMessage(message);
        setMessage("");
    }

    const handleSubmit = (event) => {
        event.preventDefault();
        submitForm();
    }

    const onEnterPress = (e) => {
        if (e.code === "Enter" && e.shiftKey === false) {
            e.preventDefault();
            submitForm();
        }
    }

    return (
        <div>
            <Form>
                <InputGroup className="mb-3">
                    <FormControl autoFocus as="textarea" rows={3}
                                 placeholder="Type your message"
                                 aria-label="Type your message"
                                 aria-describedby="basic-addon2"
                                 value={message}
                                 onKeyDown={onEnterPress}
                                 onChange={handleChange}
                    />
                    <InputGroup.Append>
                        <Button type="button"
                                variant="outline-secondary"
                                onClick={handleSubmit}>
                            <FiSend/>
                        </Button>
                    </InputGroup.Append>
                </InputGroup>
            </Form>
            <NotificationContainer/>
        </div>
    );
}

export default MessageSend;