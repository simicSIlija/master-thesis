import {useState} from "react";
import {Button, Col, Container, Form, Row} from "react-bootstrap";
import {useHistory} from 'react-router-dom'
import axios from 'axios';
import {NotificationContainer, NotificationManager} from "react-notifications";


function Home() {
    const endPointUser ="/api/interventions"; 
    let history = useHistory();
    const [id, setId] = useState("");

    const handleIdChange = event => { 
        event.preventDefault();
        setId(event.target.value);
    }

    const sendId = (event) => {
        event.preventDefault();
        if (id.trim() === "") {
            NotificationManager.error('Please enter your id', 'Your id can not be empty', 2000);
            return;
        }
        axios.post(endPointUser, createDto()).then(
            response => {
                history.push({
                    pathname:"/valid",
                    state: {intervention: response.data}
                })
            }
        )
        .catch(error => {
            console.log(error);
            NotificationManager.error('ID already tested', 'Error happened', 2000);});
    }

    const createDto = () => {
        return {
            name: id
        };
    }
    
    return (
        <div>
            <Container>
                <Row>

                    <Col className="justify-content-center text-center">
                        <h1>Read this first</h1>
                        <h2>What is this?</h2>
                        <p className="text-justify">
                           This is a research study for my master thesis,
                            where we want to investigate how people perceive information quality of articles they read.
                            First of all, you need to know, what information quality is.
                            We define information quality as a combination
                            of <i>clarity</i>, <i>unbiased information</i> and <i>expertise</i>. For purposes of this
                            study, we created three group of
                            articles: <b>LOW</b>, <b>MEDIUM</b> and <b>HIGH</b> category. Let's define them.
                        </p>
                        <p className="text-justify">
                            <b>Low</b> quality news articles tend to take a single perspective on an issue,
                            tend to have many pictures - sometimes without being necessary to explain the article
                            content. <b>Low</b> quality newspaper articles also tend to have an informal language
                            (use slang).
                            Moreover, some newspapers are known for publishing rather <b>low</b> quality
                            articles. <b>High</b> quality articles on the other hand tend to consider and
                            discuss multiple perspectives on an issue
                            and tend to have pictures that explain the content. <b>High</b> quality articles also tend
                            to use more formal language.
                            Again, some newspapers are known for publishing rather <b>high</b> quality articles.
                            In this experiment, please use the <b>low</b> and <b>high</b> accordingly as quality
                            labels,
                            as well as <b>medium</b> to characterise newspaper articles that fall in between the two.
                        </p>
                        <h2>How does this study looks like?</h2>
                        <p className="text-justify">
                         This study consists of two parts. When you submit your id, it will take you to the first part,
                            where you will get one news article in PDF form to read. If it is hard for you to read the
                            article inside your browser, you can always use Download PDF to read it on your computer.
                            When you finish reading(you got to the last page), you will get a 3 question test. Some pdfs,
                            take over whole screen width, so sometimes questions from the first part move bellow
                            the article. If you
                         answer incorrectly on those questions, you failed validation and you can not participate in the
                         study(Be careful because you will not get paid in this case). If you answer correctly, you will
                            participate
                         in study. In the end, you will see one of three intervention settings where you have to submit
                            your information quality rating and an explanation of your rating choice.
                        </p>
                        <Form onSubmit={sendId}>
                            <div>
                            <Form.Group controlId="formBasicEmail">
                                <Form.Label>Enter your ID</Form.Label>
                                <Form.Control type="text" placeholder="ID" value={id} onChange={handleIdChange} />
                            </Form.Group>

                            <Button variant="primary" type="submit">
                                Submit
                            </Button>
                            </div>
                        </Form>
                    </Col>
                </Row>
            </Container>
            <NotificationContainer/>
        </div>
    );
}

export default Home;