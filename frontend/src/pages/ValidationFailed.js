function ValidationFailed() {
    return (
        <div>
            <h1>Unfortunately, you answered incorrectly on our test questions. </h1>
            <h2>You can not participate study and you will not get paid for it.</h2>
        </div>
    );
}

export default ValidationFailed;