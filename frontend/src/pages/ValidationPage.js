import ArticleFrame from "../components/ArticleFrame";
import {Container, Row, Col, Jumbotron} from "react-bootstrap";
import {useState} from "react"
import ValidationComponent from "../components/ValidationComponent";

function ValidationPage(props) {
    const intervention = props.location.state.intervention;
    const [validationVisible, setValidationVisible] = useState(false);
    const id = intervention.article.id;
    const source = "/api/articles/pdf/" + id;

    function finishArticle() {
        setValidationVisible(true);
    }

    console.log(intervention);
    return (
        <div>
            <Container>
                <Row>
                    <Col>
                        <ArticleFrame source={source} finishArticle={finishArticle}/>
                    </Col>
                    <Col>
                        <Jumbotron>
                            {validationVisible ?
                                <ValidationComponent intervention={intervention}/> : "Read the article first"}
                        </Jumbotron>
                    </Col>
                </Row>
            </Container>
        </div>
    );

}

export default ValidationPage;