import ArticleFrame from "../components/ArticleFrame";
import SubmitAnswer from "../components/SubmitAnswer";
import { Container, Row, Col, Jumbotron } from "react-bootstrap";


function NoIntervention(props) {
    const intervention = props.location.state.intervention;
    const articleId = intervention.article.id;
    const source = "/api/articles/pdf/" + articleId;

    const finishArticle = () => void 0;

    return (
        <div>
            <Container>
                <Row>
                    <Col>
                        <ArticleFrame source={source} finishArticle={finishArticle}/>
                    </Col>
                    <Col>
                        <Jumbotron>
                            <SubmitAnswer intervention={intervention}/>
                        </Jumbotron>
                    </Col>
                </Row>
            </Container>
        </div>
    );

}

export default NoIntervention;