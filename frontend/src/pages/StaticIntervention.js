import ArticleFrame from "../components/ArticleFrame";
import { Container, Row, Col, Jumbotron } from "react-bootstrap";
import ArticleData from "../components/ArticleData";
import SubmitAnswer from "../components/SubmitAnswer";

function StaticIntervention(props) {
    const intervention = props.location.state.intervention;
    const article = intervention.article;
    const articleId = article.id;
    const source = "/api/articles/pdf/" + articleId;
    const finishArticle = () => void 0;
    return (
        <div>
            <Container>
                <Row>
                    <Col>
                        <ArticleFrame source={source} finishArticle={finishArticle}/>
                    </Col>
                    <Col>
                        <Jumbotron>
                            <ArticleData article={article}/>
                            <SubmitAnswer intervention={intervention}/>
                        </Jumbotron>
                    </Col>
                </Row>
            </Container>
        </div>
    );

}

export default StaticIntervention;