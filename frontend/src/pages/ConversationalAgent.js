import {Col, Container, Jumbotron, Row} from "react-bootstrap";
import ChatComponent from "../components/ChatComponent";
import ArticleFrame from "../components/ArticleFrame";


function ConversationalAgent(props) {
    const intervention = props.location.state.intervention;
    const articleId = intervention.article.id;
    const source = "/api/articles/pdf/" + articleId;
    console.log(intervention);
    const finishArticle = () => void 0;
    return (
        <div>
            <Container>
                <Row>
                    <Col>
                         <ArticleFrame source={source} finishArticle={finishArticle}/>
                    </Col>
                    <Col>
                        <Jumbotron>
                            <ChatComponent intervention={intervention}/>
                        </Jumbotron>
                    </Col>
                </Row>
            </Container>
        </div>
    );

}

export default ConversationalAgent;