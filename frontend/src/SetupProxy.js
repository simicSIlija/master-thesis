const {createProxyMiddleware} = require('http-proxy-middleware');

module.exports = function (app) {
    app.use(
        '/socket-io',
        createProxyMiddleware({
            target: 'http://newsagent.know-center.tugraz.at:9001',
            ws: true,
        }),
    );
};
