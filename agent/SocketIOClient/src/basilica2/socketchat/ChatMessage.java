package basilica2.socketchat;

public class ChatMessage {
    private String userName;
    private String message;

    public ChatMessage(String userName, String message) {
        this.userName = userName;
        this.message = message;
    }

    public ChatMessage() {
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
