package basilica2.socketchat;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;


public class ChatMessageDto {
    private String userName;
    private static final String USER_NAME = "userName";

    private Long interventionId;
    private static final String INTERVENTION_ID = "interventionId";

    private String content;
    private static final String CONTENT = "content";

    private Date date;
    private static final String DATE = "date";

    private String articleTopic;
    private static final String ARTICLE_TOPIC = "articleTopic";

    public ChatMessageDto(String userName, Long interventionId, String content, Date date, String articleTopic) {
        this.userName = userName;
        this.interventionId = interventionId;
        this.content = content;
        this.date = date;
        this.articleTopic = articleTopic;
    }

    public ChatMessageDto() {
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Long getInterventionId() {
        return interventionId;
    }

    public void setInterventionId(Long interventionId) {
        this.interventionId = interventionId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getArticleTopic() {
        return articleTopic;
    }

    public void setArticleTopic(String articleTopic) {
        this.articleTopic = articleTopic;
    }

    public static ChatMessageDto fromJSONObject(JSONObject jsonObject) throws JSONException {
        ChatMessageDto dto = new ChatMessageDto();
        if(jsonObject.has(USER_NAME)){
            dto.setUserName(jsonObject.getString(USER_NAME));
        }
        if(jsonObject.has(INTERVENTION_ID)){
            dto.setInterventionId(jsonObject.getLong(INTERVENTION_ID));
        }
        if(jsonObject.has(CONTENT)){
            dto.setContent(jsonObject.getString(CONTENT));
        }
        if (jsonObject.has(ARTICLE_TOPIC)){
            dto.setArticleTopic(jsonObject.getString(ARTICLE_TOPIC));
        }
        if(jsonObject.has(DATE)){
            dto.setDate(new Date());
        }
        return dto;
    }
}
