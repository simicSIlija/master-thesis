package basilica2.socketchat;

public class ChatAddUser {
    private String room;
    private String userName;

    public ChatAddUser(String room, String userName) {
        this.room = room;
        this.userName = userName;
    }

    public ChatAddUser() {
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
