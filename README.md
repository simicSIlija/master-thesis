# Master Thesis

All work about master thesis

TODO:
- [x] Add react
- [x] Connect with Spring 
- [x] Create Article component 
---
- [x] Create excel sheet for article labels
- [x] Write manual for labeling
- [x] Add POSTs for static and no intervention 
- [x] Add WebSockets Server
- [x] Add WebSockets Client 
- [ ] Update static intervention to be more neutral
- [ ] Insert agent
- [ ] Update dialog ideas in README.MD 
- [ ] Update chat interface
## Dialog ideas

After user reads the article,
he will click the button for starting conversation.

START: Hello, my name is Newsy.
 My task here is to help you to determine a quality of the article you read about $TOPIC.
In the end, you will assess the article with LOW, MEDIUM or HIGH quality grade. 
---
Q1: So, how informative was the article? 

A: In answer we look for one of article quality grades(LMH) or
 for some hint about future questions.

---

Q2: What can you say about language style? (Friends didn't understand)

_Formal/slang_
 
Same or different question for _emotive, dramatic / straight-forward_

A: Look for some of the keywords

C: Give a hint about quality correlation with language style?

---
Q3: Does the author have different perspectives or pushes only his agenda? 

A: Yes/No? 

---
Q4: Is the article dominated with pictures or a number of pictures is appropriate?

A: Two different ways of looking on this:
* Images as giving an opportunity for break
* Images as distraction
---
Q5: Did you know that this article was written by: $SOURCE, which is according to Wikipedia: $SOURCEDESTINATION? 

A: Yes/No

---
Q6: After this short conversation, how would you rate article? 

A: Chosen from dropdown menu. 

--- 
Thanks for the participation. Here is your code: _____
## Other
[Link for article database](https://docs.google.com/spreadsheets/d/19Y6xf46c6c9hW15flxqR91vLqzFJOkYZixd6qm9MnCo/edit?usp=sharing)

[Source for feature selection](https://www.bbc.co.uk/bitesize/guides/zc3nmnb/revision/2)

[And here](https://www.grin.com/document/186369)