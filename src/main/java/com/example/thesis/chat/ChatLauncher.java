package com.example.thesis.chat;

import com.corundumstudio.socketio.Configuration;
import com.corundumstudio.socketio.HandshakeData;
import com.corundumstudio.socketio.SocketIOServer;
import com.example.thesis.model.ChatMessage;
import com.example.thesis.model.communication.ChatMessageDto;
import com.example.thesis.service.ChatService;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@Component
@Log
public class ChatLauncher implements ApplicationRunner {
    public static final String updateClient = "updatechat";
    public static final String messageFromClient = "sendchat";
    public static final String addUser = "adduser";
    public static final String dumpHistory = "dump_history";
    public static final String agentName = "Newsy";


    private final ChatService chatService;

    @Autowired
    public ChatLauncher(ChatService chatService) {
        this.chatService = chatService;
    }

    @Override
    public void run(ApplicationArguments args) {
        var config = new Configuration();
        config.setHostname("newsagent.know-center.tugraz.at");
        config.setPort(8081);
        SocketIOServer server = new SocketIOServer(config);

        server.addConnectListener(client -> {
            HandshakeData handshakeData = client.getHandshakeData();
            log.info("Client[" + client.getSessionId().toString() +
                    "] - Connected to chat module through " + handshakeData.getUrl());
        });

        server.addDisconnectListener(client -> log.info("Client[" + client.getSessionId().toString() +
                "] - Disconnected from chat module. "));

        server.addEventListener(addUser, ChatAddUser.class, (client, data, ackRequest) -> {
            log.info("Client[" + data.getUserName() +
                    "] joining room: " + data.getRoom());
            var room = data.getRoom();
            var userName = data.getUserName();
            client.joinRoom(room);
            Long id = Long.valueOf(room);
            List<ChatMessage> messageList = chatService.loadMessages(id);
            if(!agentName.equals(userName)) {
                List<ChatMessageDto> dtoList = messageList.stream()
                                                          .map(chatService::convert)
                                                          .collect(Collectors.toList());
                server.getRoomOperations(room).sendEvent(dumpHistory, dtoList);
            }
           if(!agentName.equals(userName) && server.getRoomOperations(room).getClients().size() == 1){
                addAgentToChat(room);
            }
        });

        server.addEventListener(messageFromClient, ChatObject.class, (client, data, ackRequest) -> {
            log.info("Received message" + data.toString());

            var clientRooms = client.getAllRooms();
            var stringId = clientRooms
                    .stream()
                    .filter(room -> room != null && !room.equals(""))
                    .findFirst().orElse(null);
            if (stringId == null) {
                client.disconnect();
                return;
            }

            Long roomId = Long.valueOf(stringId);
            var chatMessage = chatService.saveMessage(data, roomId);
            var dto = chatService.convert(chatMessage);
            server.getRoomOperations(stringId).sendEvent(updateClient, dto);
        });

        server.start();
    }
    private void addAgentToChat(String room){
        String homeDir = System.getProperty("user.dir");
        String[] cmd = { homeDir + "/agent/NewsAgent/runtime/launch_agent.sh", room};
        try {
            Process p = Runtime.getRuntime().exec(cmd);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
