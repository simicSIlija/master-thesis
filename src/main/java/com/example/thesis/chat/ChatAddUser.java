package com.example.thesis.chat;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.java.Log;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Log
public class ChatAddUser {
    private String room;
    private String userName;

    public ChatAddUser(String json) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            ChatAddUser chatObject = mapper.readValue(json, ChatAddUser.class);
            setRoom(chatObject.getRoom());
            setUserName(chatObject.getUserName());
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }
}
