package com.example.thesis.chat;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.java.Log;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Log
public class ChatObject {
    private String userName;
    private String message;


    /**
     * For some reason, socket doesn't properly deserializes JSON, so here is
     * some workaround.
     *
     * @param json object in json form
     */
    public ChatObject(String json) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            ChatObject chatObject = mapper.readValue(json, ChatObject.class);
            setMessage(chatObject.getMessage());
            setUserName(chatObject.getUserName());
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }
}
