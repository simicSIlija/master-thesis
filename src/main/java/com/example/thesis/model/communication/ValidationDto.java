package com.example.thesis.model.communication;

import com.example.thesis.model.Article;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ValidationDto {
    private String topic;
    private String firstImage;
    private String source;

    public boolean isCorrect(Article article){
        if(topic == null || firstImage == null || source == null){
            return false;
        }
        return topic.equals(article.getTopic()) &&
                firstImage.equals(article.getFirstImage()) &&
                source.equals(article.getSource());

    }
}
