package com.example.thesis.model.communication;

import com.example.thesis.model.ArticleQuality;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SubmitAnswerDto {
    private ArticleQuality answer;
    private String explanation;
}
