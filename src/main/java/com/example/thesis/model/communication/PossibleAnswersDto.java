package com.example.thesis.model.communication;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class PossibleAnswersDto {

    private List<String> topics;
    private List<String> firstImages;
    private List<String> sources;
}
