package com.example.thesis.model.communication;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ChatMessageDto {
    private String userName;

    private Long interventionId;

    private String content;

    private Date date;

    private String articleTopic;
}
