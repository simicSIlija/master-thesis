package com.example.thesis.model.communication;

import com.example.thesis.model.User;
import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserDto {

    private String name;

    public static UserDto createDto(User user){
        UserDto userDto = new UserDto();
        userDto.setName(user.getName());
        return userDto;
    }
}
