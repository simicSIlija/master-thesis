package com.example.thesis.model;

public enum ArticlePerspective {
    SINGLE, MULTIPLE
}
