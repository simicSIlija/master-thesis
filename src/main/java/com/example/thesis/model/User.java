package com.example.thesis.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "user_table")
@Data
public class User {
    @Id
    private String name;
}
