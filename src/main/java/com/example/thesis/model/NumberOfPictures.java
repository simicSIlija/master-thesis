package com.example.thesis.model;

public enum NumberOfPictures {
    LESS, ADEQUATE, MORE
}
