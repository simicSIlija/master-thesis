package com.example.thesis.model;

public enum ArticleQuality {
    LOW, MEDIUM, HIGH
}
