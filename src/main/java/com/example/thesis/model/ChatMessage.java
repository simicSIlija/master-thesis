package com.example.thesis.model;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
public class ChatMessage {

    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne
    private User user;

    @ManyToOne
    private Intervention intervention;

    @Column(length = 10000)
    private String content;

    private Date date;
}
