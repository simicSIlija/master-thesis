package com.example.thesis.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class Article {

    @Id
    @GeneratedValue
    private Long id;

    private String title;

    private String locationOfHtmlFile;

    @Enumerated(EnumType.STRING)
    private ArticleQuality quality;

    private Boolean formalLanguageStyle;

    @Enumerated(EnumType.STRING)
    private NumberOfPictures numberOfPictures;

    @Enumerated(EnumType.STRING)
    private ArticlePerspective perspective;

    private String source;

    private String sourceDescription;

    private String topic;

    private String firstImage;

}
