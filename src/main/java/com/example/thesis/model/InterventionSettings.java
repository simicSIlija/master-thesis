package com.example.thesis.model;

public enum InterventionSettings {
    NO_INTERVENTION, STATIC_INTERVENTION, CONVERSATIONAL_AGENT
}
