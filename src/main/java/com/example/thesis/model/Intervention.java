package com.example.thesis.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class Intervention {
    @Id
    @GeneratedValue
    private Long id;

    @Enumerated(EnumType.STRING)
    private InterventionSettings interventionSettings;

    @OneToOne
    private User user;

    @ManyToOne
    private Article article;

    private boolean validated;

    @Enumerated(EnumType.STRING)
    private ArticleQuality userGuess;

    @Column(length = 10000)
    private String explanation;
}
