package com.example.thesis.repository;

import com.example.thesis.model.Article;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Set;

public interface ArticleRepository extends JpaRepository<Article, Long> {

    @Query("SELECT distinct a.topic FROM Article a")
    Set<String> getAllTopics();

    @Query("SELECT distinct a.firstImage FROM Article a")
    Set<String> getAllFirstImages();

    @Query("SELECT distinct a.source FROM Article a")
    Set<String> getAllSources();
}
