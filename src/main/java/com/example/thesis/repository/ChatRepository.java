package com.example.thesis.repository;

import com.example.thesis.model.ChatMessage;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ChatRepository extends JpaRepository<ChatMessage, Long> {
    List<ChatMessage> getALlByInterventionId(Long id);
}
