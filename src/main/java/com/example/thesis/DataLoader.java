package com.example.thesis;

import com.example.thesis.model.Article;
import com.example.thesis.model.User;
import com.example.thesis.repository.ArticleRepository;
import com.example.thesis.repository.UserRepository;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

@Component
@Log
public class DataLoader implements ApplicationRunner {
    private UserRepository userRepository;
    private ArticleRepository articleRepository;
    private ResourceLoader resourceLoader;
    public static final String AGENT_NAME = "Newsy";

    @Autowired
    public DataLoader(UserRepository userRepository, ArticleRepository articleRepository, ResourceLoader resourceLoader) {
        this.userRepository = userRepository;
        this.articleRepository = articleRepository;
        this.resourceLoader = resourceLoader;
    }

    @Override
    public void run(ApplicationArguments args) {
        User agentUser = new User();
        agentUser.setName(AGENT_NAME);
        userRepository.save(agentUser);
        loadArticles();
    }

    //TODO: Make it generic and article independent
    private void loadArticles() {
        try {
            Resource resource = resourceLoader.getResource("classpath:articles/Data.json");
            File file = resource.getFile();
            final ObjectMapper mapper = new ObjectMapper();
            List<Article> articles = mapper.readValue(file, new TypeReference<>() {
            });
            articleRepository.saveAll(articles);
        } catch (FileNotFoundException exception) {
            log.severe("File not found");
            exception.printStackTrace();
        } catch (IOException e) {
            log.severe("JSON formatting error");
            e.printStackTrace();
        }

    }
}
