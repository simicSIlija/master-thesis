package com.example.thesis.controller;

import com.example.thesis.model.ChatMessage;
import com.example.thesis.model.Intervention;
import com.example.thesis.service.ChatService;
import com.example.thesis.service.InterventionService;
import com.opencsv.CSVWriter;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

@RestController
@RequestMapping(value = "api/dashboard")
public class DashboardController {
    private final ChatService chatService;
    private final InterventionService interventionService;

    @Autowired
    public DashboardController(ChatService chatService, InterventionService interventionService) {
        this.chatService = chatService;
        this.interventionService = interventionService;
    }

    @GetMapping("/messages")
    public void getAllMessages(HttpServletResponse response) throws Exception {
        List<ChatMessage> messages = chatService.getAll();
        //set file name and content type

        String filename = "messages.csv";

        response.setContentType("text/csv");
        response.setHeader(HttpHeaders.CONTENT_DISPOSITION,
                "attachment; filename=\"" + filename + "\"");

        //create a csv writer
        StatefulBeanToCsv<ChatMessage> writer = new StatefulBeanToCsvBuilder<ChatMessage>(response.getWriter())
                .withQuotechar(CSVWriter.NO_QUOTE_CHARACTER)
                .withSeparator(CSVWriter.DEFAULT_SEPARATOR)
                .withOrderedResults(false)
                .build();

        //write all users to csv file
        writer.write(messages);
    }

    @GetMapping("/interventions")
    public void getAllInterventions(HttpServletResponse response) throws Exception {
        List<Intervention> interventions = interventionService.getAll();
        //set file name and content type

        String filename = "interventions.csv";

        response.setContentType("text/csv");
        response.setHeader(HttpHeaders.CONTENT_DISPOSITION,
                "attachment; filename=\"" + filename + "\"");

        //create a csv writer
        StatefulBeanToCsv<Intervention> writer = new StatefulBeanToCsvBuilder<Intervention>(response.getWriter())
                .withQuotechar(CSVWriter.NO_QUOTE_CHARACTER)
                .withSeparator(CSVWriter.DEFAULT_SEPARATOR)
                .withOrderedResults(false)
                .build();

        //write all users to csv file
        writer.write(interventions);
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE,
    value = "/json/interventions")
    public ResponseEntity<?> getAllInterventionsJSON() {
        var interventionList = interventionService.getAll();
        return new ResponseEntity<>(interventionList, HttpStatus.OK);
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE,
            value = "/json/messages")
    public ResponseEntity<?> getAllMessagesJSON() {
        List<ChatMessage> messages = chatService.getAll();
        return new ResponseEntity<>(messages, HttpStatus.OK);
    }
}
