package com.example.thesis.controller;

import com.example.thesis.model.Intervention;
import com.example.thesis.model.communication.SubmitAnswerDto;
import com.example.thesis.model.communication.UserDto;
import com.example.thesis.service.InterventionService;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(value = "api/interventions",
        produces = APPLICATION_JSON_VALUE)
@Log
public class InterventionController {

    private final InterventionService interventionService;

    @Autowired
    public InterventionController(InterventionService interventionService) {
        this.interventionService = interventionService;
    }

    @PostMapping(consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity<?> createNewIntervention(@RequestBody UserDto userDto) {
        Intervention intervention = interventionService.createNewIntervention(userDto);
        return new ResponseEntity<>(intervention, HttpStatus.CREATED);
    }

    @PostMapping(value = "/{id}", consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity<?> submitAnswer(@RequestBody SubmitAnswerDto submitAnswerDto, @PathVariable long id){
        var correctAnswer = interventionService.submitAnswer(submitAnswerDto, id);
        return correctAnswer ?  ResponseEntity.ok().body("Success") : ResponseEntity.ok().body("Fail");
    }
}
