package com.example.thesis.controller;


import com.example.thesis.controller.exceptions.BadRequestException;
import com.example.thesis.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.ByteArrayInputStream;

@RestController
@RequestMapping(value = "api/articles")
public class ArticleController {

    private final ArticleService articleService;

    @Autowired
    public ArticleController(ArticleService articleService) {
        this.articleService = articleService;
    }

    @GetMapping
    public ResponseEntity<?> getAllArticles() {
        var articleList = articleService.getAll();
        return new ResponseEntity<>(articleList, HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<?> getSpecificArticle(@PathVariable long id){
        var article = articleService.getArticle(id);
        return new ResponseEntity<>(article, HttpStatus.OK);
    }

    @GetMapping(value = "/pdf/{id}",
            produces = MediaType.APPLICATION_PDF_VALUE)
    public ResponseEntity<InputStreamResource> getArticlePage(@PathVariable long id) {
        try {
            ByteArrayInputStream byteArrayInputStream = articleService.getArticlePage(id);
            var headers = new HttpHeaders();
            headers.add("Content-Disposition", "inline; filename=article.pdf");
            return ResponseEntity
                    .ok()
                    .headers(headers)
                    .contentType(MediaType.APPLICATION_PDF)
                    .body(new InputStreamResource(byteArrayInputStream));
        } catch (Exception e) {
            throw new BadRequestException("Wrong article id:" + id + ".");
        }

    }
}
