package com.example.thesis.controller;

import com.example.thesis.model.communication.PossibleAnswersDto;
import com.example.thesis.model.communication.ValidationDto;
import com.example.thesis.service.ValidationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Collections;

@RestController
@RequestMapping(value = "api/validation")
public class ValidationController {

    private final ValidationService validationService;

    @Autowired
    public ValidationController(ValidationService validationService) {
        this.validationService = validationService;
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<?> getPossibleAnswers(@PathVariable long id) {
        var topics = new ArrayList<>(validationService.getTopicAnswers(id));
        var imageAnswers = new ArrayList<>(validationService.getImageAnswers(id));
        var sourceAnswers = new ArrayList<>(validationService.getSourceAnswers(id));
        Collections.shuffle(topics);
        Collections.shuffle(imageAnswers);
        Collections.shuffle(sourceAnswers); //TODO: is it necessary?
        PossibleAnswersDto possibleAnswersDto = new PossibleAnswersDto(topics, imageAnswers, sourceAnswers);
        return new ResponseEntity<>(possibleAnswersDto, HttpStatus.OK);
    }

    @PostMapping(value = "/{id}",
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> validateAnswers(@PathVariable long id, @RequestBody ValidationDto validationDto) {
        var isValidated = validationService.validateAnswers(id, validationDto);
        return isValidated ? ResponseEntity.ok().body("Success") : ResponseEntity.badRequest().body("Fail");
    }

}
