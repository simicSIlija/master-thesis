package com.example.thesis.controller;

import com.example.thesis.model.User;
import com.example.thesis.model.communication.UserDto;
import com.example.thesis.service.UserService;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "api/users",
                produces = MediaType.APPLICATION_JSON_VALUE)
@Log
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    public ResponseEntity<?> getAllUsers() {
        List<UserDto> users = userService.getAll();
        return new ResponseEntity<>(users, HttpStatus.OK);
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> addNewUser(@RequestBody UserDto userDto) {
        User user = userService.addNewUser(userDto);
        return new ResponseEntity<>(user, HttpStatus.CREATED);
    }
}
