package com.example.thesis.service;

import com.example.thesis.model.Intervention;
import com.example.thesis.model.InterventionSettings;
import com.example.thesis.model.communication.SubmitAnswerDto;
import com.example.thesis.model.communication.UserDto;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface InterventionService {
    InterventionSettings determineIntervention();

    Intervention createNewIntervention(UserDto idDto);

    boolean submitAnswer(SubmitAnswerDto answerDto, long id);

    List<Intervention> getAll();
}
