package com.example.thesis.service;

import com.example.thesis.model.Article;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.util.List;


@Service
public interface ArticleService {

    Article getRandomArticle();

    ByteArrayInputStream getArticlePage(Long id);

    List<Article> getAll();

    Article getArticle(long id);
}
