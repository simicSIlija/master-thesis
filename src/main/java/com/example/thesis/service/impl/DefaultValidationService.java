package com.example.thesis.service.impl;

import com.example.thesis.controller.exceptions.BadRequestException;
import com.example.thesis.model.communication.ValidationDto;
import com.example.thesis.repository.ArticleRepository;
import com.example.thesis.repository.InterventionRepository;
import com.example.thesis.service.ValidationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;
import java.util.stream.Collectors;

@Service
public class DefaultValidationService implements ValidationService {

    private final ArticleRepository articleRepository;
    private final InterventionRepository interventionRepository;

    @Autowired
    public DefaultValidationService(ArticleRepository articleRepository, InterventionRepository interventionRepository) {
        this.articleRepository = articleRepository;
        this.interventionRepository = interventionRepository;
    }

    private int numberOfAnswers = 3;

    @Override
    public Set<String> getTopicAnswers(long id) {
        var possibleAnswers = articleRepository.getAllTopics().stream().limit(numberOfAnswers).collect(Collectors.toSet());
        var article = articleRepository.findById(id).orElseThrow(() -> new BadRequestException("Article with that id does not exist"));
        var trueAnswer = article.getTopic();
        if (!possibleAnswers.contains(trueAnswer)){
            possibleAnswers.remove(possibleAnswers.iterator().next()); // a bit hacky
            possibleAnswers.add(trueAnswer);
        }
        return possibleAnswers;
    }

    @Override
    public Set<String> getImageAnswers(long id) {
        var possibleAnswers = articleRepository.getAllFirstImages().stream().limit(numberOfAnswers).collect(Collectors.toSet());
        var article = articleRepository.findById(id).orElseThrow(() -> new BadRequestException("Article with that id does not exist"));
        var trueAnswer = article.getFirstImage();
        if (!possibleAnswers.contains(trueAnswer)){
            possibleAnswers.remove(possibleAnswers.iterator().next());
            possibleAnswers.add(trueAnswer);
        }
        return possibleAnswers;
    }

    @Override
    public Set<String> getSourceAnswers(long id) {
        var possibleAnswers = articleRepository.getAllSources().stream().limit(numberOfAnswers).collect(Collectors.toSet());
        var article = articleRepository.findById(id).orElseThrow(() -> new BadRequestException("Article with that id does not exist"));
        var trueAnswer = article.getSource();
        if (!possibleAnswers.contains(trueAnswer)){
            possibleAnswers.remove(possibleAnswers.iterator().next());
            possibleAnswers.add(trueAnswer);
        }
        return possibleAnswers;
    }

    @Override
    public boolean validateAnswers(long id, ValidationDto validationDto) {
        var intervention = interventionRepository.findById(id).orElseThrow(() -> new BadRequestException("Article with that id does not exist"));
        var isValidated = validationDto.isCorrect(intervention.getArticle());
        intervention.setValidated(isValidated);
        interventionRepository.save(intervention);
        return isValidated;
    }
}
