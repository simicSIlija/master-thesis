package com.example.thesis.service.impl;

import com.example.thesis.chat.ChatObject;
import com.example.thesis.model.ChatMessage;
import com.example.thesis.model.communication.ChatMessageDto;
import com.example.thesis.repository.ChatRepository;
import com.example.thesis.repository.InterventionRepository;
import com.example.thesis.repository.UserRepository;
import com.example.thesis.service.ChatService;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
@Log
public class DefaultChatService implements ChatService {
    private final ChatRepository chatRepository;
    private final UserRepository userRepository;
    private final InterventionRepository interventionRepository;

    @Autowired
    public DefaultChatService(ChatRepository chatRepository, UserRepository userRepository, InterventionRepository interventionRepository) {
        this.chatRepository = chatRepository;
        this.userRepository = userRepository;
        this.interventionRepository = interventionRepository;
    }

    @Override
    public ChatMessage saveMessage(ChatObject chatObject, Long roomId) {
        var user = userRepository
                .findByName(chatObject.getUserName())
                .orElseThrow(() -> new RuntimeException("No user with that id: " + chatObject.getUserName()));
        var intervention = interventionRepository
                .findById(roomId)
                .orElseThrow(() -> new RuntimeException("No intervention with that id: " + roomId));
        ChatMessage message = new ChatMessage();
        message.setUser(user);
        message.setIntervention(intervention);
        message.setContent(chatObject.getMessage());
        message.setDate(new Date());
        return chatRepository.save(message);
    }

    @Override
    public List<ChatMessage> loadMessages(Long roomId) {
        return chatRepository.getALlByInterventionId(roomId);
    }

    @Override
    public ChatMessageDto convert(ChatMessage chatMessage) {
        var dto = new ChatMessageDto();
        dto.setDate(chatMessage.getDate());
        dto.setContent(chatMessage.getContent());
        dto.setUserName(chatMessage.getUser().getName());
        dto.setInterventionId(chatMessage.getIntervention().getId());
        dto.setArticleTopic(chatMessage.getIntervention().getArticle().getTopic());
        return dto;
    }

    @Override
    public List<ChatMessage> getAll() {
        return chatRepository.findAll();
    }
}
