package com.example.thesis.service.impl;

import com.example.thesis.controller.exceptions.BadRequestException;
import com.example.thesis.model.Article;
import com.example.thesis.repository.ArticleRepository;
import com.example.thesis.service.ArticleService;
import lombok.extern.java.Log;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@Log
public class DefaultArticleService implements ArticleService {

    private final ArticleRepository repository;
    private ResourceLoader resourceLoader;

    @Autowired
    public DefaultArticleService(ArticleRepository repository, ResourceLoader resourceLoader) {
        this.repository = repository;
        this.resourceLoader = resourceLoader;
    }

    @Override
    public Article getRandomArticle() {
        List<Article> articles = repository.findAll();
        if (articles.isEmpty()) {
            throw new BadRequestException("There are no articles in database.");
        }

        int index = new Random().nextInt(articles.size());
        return articles.get(index);
    }

    //TODO: Extract file loading logic, also in {@link com.example.thesis.DataLoader}
    @Override
    public ByteArrayInputStream getArticlePage(Long id) {
        var article = repository.findById(id).orElseThrow(() -> new BadRequestException("Wrong article id."));
        try {
            Resource resource = resourceLoader.getResource("classpath:articles/" + article.getLocationOfHtmlFile());
            var inputStream = resource.getInputStream();
            return new ByteArrayInputStream(IOUtils.toByteArray(inputStream));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<Article> getAll() {
        return repository.findAll();
    }

    @Override
    public Article getArticle(long id) {
        return repository.findById(id).orElseThrow(() ->
                new BadRequestException("Article with " + id + " does not exist"));
    }
}
