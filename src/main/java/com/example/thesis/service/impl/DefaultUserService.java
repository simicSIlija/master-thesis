package com.example.thesis.service.impl;

import com.example.thesis.controller.exceptions.BadRequestException;
import com.example.thesis.model.User;
import com.example.thesis.model.communication.UserDto;
import com.example.thesis.repository.UserRepository;
import com.example.thesis.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class DefaultUserService implements UserService {

    private final UserRepository userRepository;

    @Autowired
    public DefaultUserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public List<UserDto> getAll() {
        List<User> users = userRepository.findAll();
        return users.stream().map(UserDto::createDto).collect(Collectors.toList());
    }

    @Override
    public User addNewUser(UserDto userDto) {
        String name = userDto.getName();
        Optional<User> user = userRepository.findByName(name);
        if (user.isPresent()) {
            throw new BadRequestException("Name " + name + " already taken");
        }
        User newUser = new User();
        newUser.setName(name);
        newUser = userRepository.save(newUser);
        return newUser;
    }

}
