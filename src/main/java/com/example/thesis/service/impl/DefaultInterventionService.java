package com.example.thesis.service.impl;

import com.example.thesis.controller.exceptions.BadRequestException;
import com.example.thesis.model.Intervention;
import com.example.thesis.model.InterventionSettings;
import com.example.thesis.model.communication.SubmitAnswerDto;
import com.example.thesis.model.communication.UserDto;
import com.example.thesis.repository.InterventionRepository;
import com.example.thesis.service.ArticleService;
import com.example.thesis.service.InterventionService;
import com.example.thesis.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DefaultInterventionService implements InterventionService {
    private final UserService userService;
    private final ArticleService articleService;
    private final InterventionRepository repository;

    @Autowired
    public DefaultInterventionService(UserService userService, ArticleService articleService, InterventionRepository repository) {
        this.userService = userService;
        this.articleService = articleService;
        this.repository = repository;
    }

    @Override
    public InterventionSettings determineIntervention() {
        double chance = Math.random();
        if (chance < 0.33) {
            return InterventionSettings.NO_INTERVENTION;
        }
        if (chance < 0.67) {
            return InterventionSettings.STATIC_INTERVENTION;
        }
        return InterventionSettings.CONVERSATIONAL_AGENT;
    }

    @Override
    public Intervention createNewIntervention(UserDto userDto) {
        var user = userService.addNewUser(userDto);
        var randomArticle = articleService.getRandomArticle();
        var settings = determineIntervention();
        var newIntervention = new Intervention();
        newIntervention.setArticle(randomArticle);
        newIntervention.setInterventionSettings(settings);
        newIntervention.setUser(user);
        return repository.save(newIntervention);
    }

    @Override
    public boolean submitAnswer(SubmitAnswerDto answerDto, long id) {
        var intervention = repository.findById(id).orElseThrow(() -> new BadRequestException("Intervention doesn't exist."));
        if (!intervention.isValidated()) {
            throw new BadRequestException("Intervention is not validated.");
        }
        var articleQuality = intervention.getArticle().getQuality();
        intervention.setUserGuess(answerDto.getAnswer());
        intervention.setExplanation(answerDto.getExplanation());
        repository.save(intervention);
        return articleQuality == answerDto.getAnswer();
    }

    @Override
    public List<Intervention> getAll() {
        return repository.findAll();
    }
}
