package com.example.thesis.service;

import com.example.thesis.model.User;
import com.example.thesis.model.communication.UserDto;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface UserService {

    List<UserDto> getAll();

    User addNewUser(UserDto userDto);
}
