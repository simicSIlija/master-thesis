package com.example.thesis.service;

import com.example.thesis.chat.ChatObject;
import com.example.thesis.model.ChatMessage;
import com.example.thesis.model.communication.ChatMessageDto;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ChatService {
    ChatMessage saveMessage(ChatObject chatObject, Long roomId);

    List<ChatMessage> loadMessages(Long roomId);

    ChatMessageDto convert(ChatMessage chatMessage);

    List<ChatMessage> getAll();
}
