package com.example.thesis.service;

import com.example.thesis.model.communication.ValidationDto;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public interface ValidationService {

    Set<String> getTopicAnswers(long id);

    Set<String> getImageAnswers(long id);

    Set<String> getSourceAnswers(long id);

    boolean validateAnswers(long id, ValidationDto validationDto);
}
